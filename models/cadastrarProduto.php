<?php

include('../database/conexao.php');
include('../controllers/Produto.class.php');

if(isset($_POST['save-product']))
{

    $nome  = mysqli_real_escape_string($conexao, $_POST['name']);
    $sku   = mysqli_real_escape_string($conexao, $_POST['sku']);
    $preco = mysqli_real_escape_string($conexao, $_POST['preco']);
    $quantidade = mysqli_real_escape_string($conexao, $_POST['quantity']);
    $descricao  = mysqli_real_escape_string($conexao, $_POST['description']);

    $produto = new Produto();

    $produto->setCadastrar($nome, $sku, $preco, $quantidade, $descricao);
}